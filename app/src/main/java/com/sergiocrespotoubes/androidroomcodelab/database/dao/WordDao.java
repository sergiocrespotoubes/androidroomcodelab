package com.sergiocrespotoubes.androidroomcodelab.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.sergiocrespotoubes.androidroomcodelab.database.entities.Word;

import java.util.List;

/**
 * Created by Sergio Crespo Toubes on 12/10/2018.
 * SergioCrespoToubes@gmail.com
 * www.SergioCrespoToubes.com
 */
@Dao
public interface WordDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Word word);

    @Query("DELETE FROM word_table")
    void deleteAll();

    @Query("SELECT * from word_table ORDER BY word ASC")
    LiveData<List<Word>> getAllWords();

}